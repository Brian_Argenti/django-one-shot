from django.contrib import admin
from .models import TodoList, TodoItem

# Register your models here.


@admin.register(TodoList)
class todo_list_display(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]


@admin.register(TodoItem)
class todo_item_display(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
