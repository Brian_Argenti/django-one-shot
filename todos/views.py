from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from todos.forms import ToDoForm, ToDoItemForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
        }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_instance = get_object_or_404(TodoList, id=id)
    context = {"todo_instance": todo_instance}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = ToDoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = ToDoForm()
        context = {
            "form": form,
                   }
        return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ToDoForm(request.POST, instance=todo_instance)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.id)

    else:
        form = ToDoForm(instance=todo_instance)

    context = {
        "todo_instance": todo_instance,
        "form": form
    }

    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ToDoItemForm(request.POST)
        if form.is_valid():
            item_instance = form.save()
            return redirect("todo_list_detail", id=item_instance.list.id)

    else:
        form = ToDoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    item_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ToDoItemForm(request.POST, instance=item_instance)
        if form.is_valid():
            item_instance = form.save()
            return redirect("todo_list_detail", id=item_instance.list.id)

    else:
        form = ToDoItemForm(instance=item_instance)

    context = {
        "item_instance": item_instance,
        "form": form
    }

    return render(request, "todos/items/update.html", context)
